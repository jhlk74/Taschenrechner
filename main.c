#include <stdio.h>
#include <stdbool.h>

typedef struct{
    double z;
    double n;
    double d;
}BRUCH;

int kuerzen(BRUCH B)
{
    int h;
    int a = B.z;
    int b = B.n;
    
    while(b != 0)
    {
        h = a % b;
        a = b;
        b = h;
    }
    
    return a;
}

BRUCH Kehrwert(BRUCH B)
{
    BRUCH final;
    
    final.z = B.n;
    final.n = B.z;
    
    final.d = final.z/final.n;
    
    printf("Kehrwert: %d / %d = %f\n", (int)final.z, (int)final.n, final.d);
    
    return final;
}

BRUCH Bruchadd(BRUCH b1, BRUCH b2)
{
    BRUCH nfinal;       //ungekürzt
    BRUCH final;        //gekürzt
    int t;
    
    nfinal.z = (b1.z * b2.n) + (b2.z * b1.n);
    nfinal.n = b1.n * b2.n;
    
    t = kuerzen(nfinal);
    
    final.z = nfinal.z/t;
    final.n = nfinal.n/t;
    
    final.d = final.z/final.n;
    
    printf("Add:      %d / %d = %f\n", (int)final.z, (int)final.n, final.d);
    Kehrwert(final);
    
    return final;
}

BRUCH Bruchsub(BRUCH b1, BRUCH b2)
{
    BRUCH nfinal;
    BRUCH final;
    
    int t;

    nfinal.z = (b1.z * b2.n) - (b2.z * b1.n);
    nfinal.n = b1.n * b2.n;
    
    t = kuerzen(nfinal);
    
    final.z = nfinal.z/t;
    final.n = nfinal.n/t;
    
    final.d = final.z/final.n;
    
    printf("Sub:      %d / %d = %f\n", (int)final.z, (int)final.n, final.d);
    Kehrwert(final);
    
    return final;
}

BRUCH Bruchmul(BRUCH b1, BRUCH b2)
{
    BRUCH nfinal;
    BRUCH final;
    
    int t;
    
    nfinal.z = b1.z * b2.z;
    nfinal.n = b1.n * b2.n;
    
    t = kuerzen(nfinal);
    
    final.z = nfinal.z/t;
    final.n = nfinal.n/t;
    
    final.d = final.z/final.n;
    
    printf("Mul:      %d / %d = %f\n", (int)final.z, (int)final.n, final.d);
    Kehrwert(final);
    
    return final;
}

BRUCH Bruchdiv(BRUCH b1, BRUCH b2)
{
    BRUCH nfinal;
    BRUCH final;
    
    int t;
    
    nfinal.z = b1.z * b2.n;
    nfinal.n = b1.n * b2.z;
    
    t = kuerzen(nfinal);
    
    final.z = nfinal.z/t;
    final.n = nfinal.n/t;
    
    final.d = final.z/final.n;
    
    printf("Div:      %d / %d = %f\n", (int)final.z, (int)final.n, final.d);
    Kehrwert(final);
    
    return final;
}

void input()
{
    int z1, z2, n1, n2, i;
    bool done = false;
    
    printf("Zähler von Bruch 1: ");         //Bruch1 Eingabe
    scanf("%d", &z1);
    printf("Nenner von Bruch 1: ");
    scanf("%d", &n1);
    
    printf("Zähler von Bruch 2: ");         //Bruch2 Eingabe
    scanf("%d", &z2);
    printf("Nenner von Bruch 2: ");
    scanf("%d", &n2);
    
    BRUCH b1 = {z1, n1};
    BRUCH b2 = {z2, n2};
    
    do
    {
        printf("\nGib die jeweilige Zahl ein:\n1: Addition \n2: Subtraktion \n3: Multiplikation \n4: Division\n");
        scanf("%d", &i);
        
        switch(i)
        {
            case 1:  printf("Addition:\n");          Bruchadd(b1, b2); done = true; break;
            case 2:  printf("Subtraktion:\n");       Bruchsub(b1, b2); done = true; break;
            case 3:  printf("Multiplikation:\n");    Bruchmul(b1, b2); done = true; break;
            case 4:  printf("Division:\n");          Bruchdiv(b1, b2); done = true; break;
            default: printf("Gib 1, 2, 3 oder 4 ein:\n"); break;
        }
    }while(!done);
}

int main(int argc, const char * argv[]) {
    
    input();
    
    return 0;
}
